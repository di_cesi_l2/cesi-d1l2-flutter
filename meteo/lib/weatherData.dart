



class WeatherData {
  final String main;
  final String description;

  WeatherData({required this.main, required this.description});

  factory WeatherData.fromJson(Map<String, dynamic> json) {

    return WeatherData(
      main: json['weather'][0]['main'] as String,
      description: json['weather'][0]['description'] as String,
     
    );
  }
}

  