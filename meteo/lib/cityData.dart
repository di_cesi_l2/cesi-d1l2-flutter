class CityData {
    CityData({required this.latitude, required this.longitude}); 

    final double latitude;
    final double longitude;



    factory CityData.fromJson(Map<String, dynamic>json){
      return CityData(
        latitude: json['latitude'] as double,
        longitude: json['longitude'] as double
      );

    }
  

    }
  
