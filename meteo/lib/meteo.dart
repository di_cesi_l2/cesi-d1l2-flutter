import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';


import 'package:dio/dio.dart';
import 'package:meteo/cityData.dart';
import 'package:meteo/weatherData.dart';

class Meteo extends StatefulWidget {
  const Meteo({super.key, required this.title});

  final String title;

  @override
  State<Meteo> createState() => _Meteo();
}

class _Meteo extends State<Meteo> {
   final TextEditingController _cityController = TextEditingController();
   
    CityData? _cityData;
    WeatherData? _weatherData;

  Future<void>getWeatherInfo(double latitude, double longitude) async{

    final dio = Dio();
    String? appid = dotenv.env['METEO_API_KEY'];
    final url = 'https://api.openweathermap.org/data/2.5/weather?lat=48.6936&lon=6.1846&appid=7d730b4798088a2d0d8e3d4a67059633';
    
    print(dotenv.env['METEO_API_KEY']);

    try{
      final response = await dio.get(url);
    if (response.statusCode == 200) {
      
      setState(() {
        _weatherData = WeatherData.fromJson(response.data);
      
      });
      
      } else {
        print('Failed to get weather info: ${response.statusCode}');
      }
    }
    catch (e) {
      print('Error retrieving weather info: $e');
    }
    print(_weatherData);
}












Future<void>getCityInfo() async {
  try {
    
    String city = _cityController.text;
    print(city);
    final dio = Dio();
    final url = 'https://api.api-ninjas.com/v1/city?name=$city';
    print(dotenv.env['CITY_API_KEY']);
    dio.options.headers['X-Api-Key'] = dotenv.env['CITY_API_KEY'];
    
    final response = await dio.get(url);
    if (response.statusCode == 200) {
      
      setState(() {
        _cityData = CityData.fromJson(response.data[0]);
        print(_cityData);
      });
      getWeatherInfo(_cityData!.latitude, _cityData!.longitude);
    } else {
      print('Failed to get city info: ${response.statusCode}');
    }
  } catch (e) {
    print('Error retrieving city info: $e');
  }
  
}
  


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        backgroundColor: Theme.of(context).colorScheme.inversePrimary,

        title: Text(widget.title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('Renseigner une ville'),
              SizedBox(
                height: 300,
                width: 300,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _cityController,
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                
                  getCityInfo();
                },
                child: const Text('Valider'),
              ),
              _cityData != null ? Text(_cityData!.latitude.toString()):Container(),
              _cityData != null ? Text(_cityData!.longitude.toString()):Container(),
              _weatherData != null ? Text(_weatherData!.main.toString()):Container(),
              _weatherData != null ? Text(_weatherData!.description.toString()):Container()

            ],
          ),

          
        )
          
    );
  }
}
