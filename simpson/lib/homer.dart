import 'package:flutter/material.dart';
import 'bart.dart';
import 'message.dart';

class Homer extends StatefulWidget {
  const Homer({super.key, required this.title});

  final String title;

  @override
  State<Homer> createState() => _HomerState();
}

class _HomerState extends State<Homer> {
  final TextEditingController _homerMessageToBartController = TextEditingController();
  String _message = '';
  String _bartMessage = '';

  void _sendMessageToBart() {
    setState(() {
      _message = _homerMessageToBartController.text;
    });
  }

  void _onBartCall(String value) {
    setState(() {
      _bartMessage = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              Image.asset('images/Homer.png', height: 150, width: 80),
              SizedBox(
                height: 300,
                width: 300,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _homerMessageToBartController,
                  ),
                ),
              ),
              Message(message: _bartMessage),
              ElevatedButton(
                onPressed: () {
                  // Transmettre le message à Bart
                  _sendMessageToBart();
                },
                child: const Text('Valider'),
              ),
            ],
          ),
          Bart(hommerMessage: _message, callHomer: _onBartCall),
        ],
      ),
    );
  }
}
