    import 'package:flutter/material.dart';
    import 'message.dart';
    class Bart extends StatelessWidget {
      const Bart({super.key, required this.hommerMessage, required this.callHomer}); // Correction de la syntaxe du constructeur
      final bartMessage = 'Bonjour Homer';
      final String hommerMessage; // Spécification du type de paramètre message
      final Function(String) callHomer;
      
      @override
      Widget build(BuildContext context) {
        return 
        Column(
                children: [
        Image.asset('images/Bart.png',height: 150, width: 80),
        Message(message: hommerMessage),
        
              
        ElevatedButton(
                onPressed: () => callHomer(bartMessage),
                child: const Text('Valider'),
                )
                
              
                ]);
      }
    }
