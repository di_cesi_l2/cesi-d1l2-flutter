import 'dart:developer';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  List<Widget> _listItems = [];

  Widget getFruitImage(int number) {
    if (isPrime(number)) {
      return Image.asset('images/ananas.png', width: 30, height: 30);
    } else if (number % 2 == 0) {
      return Image.asset('images/poire.png', width: 30, height: 30);
    } else {
      return Image.asset('images/pomme.png', width: 30, height: 30);
    }
  }

  bool isPrime(int number) {
    if (number <= 1) return false;
    for (int i = 2; i <= number / 2; i++) {
      if (number % i == 0) return false;
    }
    return true;
  }

  Color getColor(int number) {
    return number % 2 == 0 ? Colors.indigo : Colors.cyan;
  }

  String getType(int number) {
    if (isPrime(number)) {
      return 'Nombre premier';
    } else if (number % 2 == 0) {
      return 'Pair';
    } else {
      return 'Impair';
    }
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
      _listItems.add(
        ListTile(
          leading: getFruitImage(_counter),
          title: Row(
            children: [
              Text(
                '$_counter ',
                style: const TextStyle(color: Colors.white),
              ),
              Text(
                getType(_counter),
                style: const TextStyle(color: Colors.white),
              ),
            ],
          ),
          tileColor: getColor(_counter),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Row(
          children: [
            Text('$_counter ', style: const TextStyle(fontSize: 20)),
            Text(
              getType(_counter),
              style: const TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
      body: ListView(
        children: <Widget>[
          const Text(
            'Vous avez ajouté les items suivants à la liste :',
          ),
          ..._listItems,
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        backgroundColor: getColor(_counter),
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
