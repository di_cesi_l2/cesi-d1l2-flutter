class User {
  String ?accessToken;
  String ?refreshToken;
  String ?userId;
  int ?expires;
  
  String ?firstname;
  String ?lastname;

  User({
    this.accessToken,
    this.refreshToken,
    this.firstname,
    this.lastname,
    this.userId,
    this.expires,
  });

  factory User.fromSignIn(Map<String, dynamic> json) => User(
    accessToken: json['access_token'],
    refreshToken: json['refresh_token'],
    expires: json['expires'],
  );

  factory User.fromSignUp(Map<String, dynamic> json, User user) {
    user.firstname = json['first_name'];
    user.lastname = json['last_name'];
    return user;
  }

  factory User.fromUserInfo(Map<String, dynamic> json, User user) {
    user.firstname = json['first_name'];
    user.lastname = json['last_name'];
    user.userId = json['id'];
    return user;
  }
}



