import 'package:dio/dio.dart';

class Pizzas {
  String id;
  String name;
  String image;
  String category;
  List ingredients;
  int price;
  Pizzas({
    required this.id,
    required this.name,
    required this.image,
    required this.category,
    required this.ingredients,
    required this.price
  });

  factory Pizzas.fromJson(Map<String, dynamic> json) => Pizzas(
        id: json["id"],
        name: json["name"],
        image: 'https://pizzas.shrp.dev/assets/' + json['image'],
        category: json['category'],
        ingredients: json['ingredients'],
        price: json['price']
      );
}

Future<List<Pizzas>> getPizzas() async {

  final dio = Dio();
  const url = 'https://pizzas.shrp.dev/items/pizzas';
  final response = await dio.get(url);
    if (response.statusCode == 200) {


   
      List<dynamic> jsonResponse = response.data['data'];


      List<Pizzas> pizzas = [];
      for (var u in jsonResponse) {
        Pizzas pizza = Pizzas.fromJson(u
        );
        pizzas.add(pizza);
        
      }
    return pizzas;
  } else {
    throw Exception('Erreur lors de la récupération des pizzas');
  }
}

