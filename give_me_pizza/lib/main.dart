
import 'package:give_me_pizza/cartProvider.dart';
import 'package:flutter/material.dart';
import 'package:give_me_pizza/pizzaList.dart';
import 'package:give_me_pizza/user_provider.dart';
import 'package:provider/provider.dart';
void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => Cart()),
        ChangeNotifierProvider(create: (context) => UserProvider())
      ]
      ,
      child: const MyApp(),
    ),
  );
}


class MyApp extends StatelessWidget {
  const MyApp({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home:(const PizzaList(title: "Pizza")));
    
  }
}



