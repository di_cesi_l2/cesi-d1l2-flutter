import 'dart:collection';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:give_me_pizza/user.dart';


class UserProvider extends ChangeNotifier {
  User user = User();

  signuptest(email,password){//juste pour tester si on récupére bien des infos depuis le formulaire
    print(email);
  }
 

  Future<void> signup(email,password,firstname,lastname) async {

    final data = {
    'role':"bad526d9-bc5a-45f1-9f0b-eafadcd4fc15",
      'email': email,
      'password': password,
      'fisrt_name':firstname,
      'last_name':lastname
    };
  print(data);
    try {
      final Response response = await Dio().post(
        'https://pizzas.shrp.dev/users',
        data: data,
      );
      if (response.statusCode == 200) {
      } else {
        throw Exception('Erreur lors de l\'inscription');
      }
      print(response.statusCode);
    } catch (error) {
      print('Erreur lors de la requête: $error');
      throw Exception('Erreur lors de l\'inscription');
    }

  }

    Future<void> signin(email,password) async {

    final data = {
 
      'email': email,
      'password': password
    };

    try {
      final Response response = await Dio().post(
        'https://pizzas.shrp.dev/auth/login',
        data: data,
      );
      if (response.statusCode == 200) {
        user = User.fromSignIn(response.data['data']);
          print(response.data['data']);
          
        await getUserInfos(user.accessToken);
        notifyListeners();
        print(user.accessToken);
      } else {
        throw Exception('Erreur lors de la connexion');
      }
      print(response.statusCode);
    } catch (error) {
      print('Erreur lors de la requête: $error');
      throw Exception('Erreur lors de la connexion');
    }

  }

Future<void> getUserInfos(String ?accessToken) async {
  final dio = Dio();
  
  try {
    final Response response = await dio.get(
      'https://pizzas.shrp.dev/users/me',
      options: Options(headers: {'Authorization': 'Bearer $accessToken'}),
    );

    if (response.statusCode == 200) {
      user = User.fromUserInfo(response.data['data'], user);
      notifyListeners();
    } else {
      throw Exception('Erreur lors de la récupération des informations utilisateur');
    }
  } catch (error) {
    print('Erreur lors de la requête: $error');
    throw Exception('Erreur lors de la récupération des informations utilisateur');
  }
}

}












