import 'package:flutter/material.dart';
import 'package:give_me_pizza/cartDetails.dart';
import 'package:give_me_pizza/cartProvider.dart';
import 'package:give_me_pizza/sign_in.dart';
import 'package:give_me_pizza/sign_up.dart';
import 'package:give_me_pizza/user_provider.dart'; // Importez le UserProvider
import 'package:provider/provider.dart';

class CustomNavbar {
  AppBar getAppBar(BuildContext context, String title) {
    return AppBar(
      title: Text(title),
      actions: [
        Consumer<UserProvider>(
          builder: (context, userProvider, child) {
            if ( userProvider.user.firstname != null && userProvider.user.lastname != null) {
              return Text('${userProvider.user.firstname} ${userProvider.user.lastname}');
            } else {
              return Row(
                children: [
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const SigninForm(),
                        ),
                      );
                    },
                    child: const Text('Se connecter'),
                  ),
                  const SizedBox(width: 16), // Espacement entre les boutons
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const SignupForm(),
                        ),
                      );
                    },
                    child: const Text('S\'inscrire'),
                  ),
                ],
              );
            }
          },
        ),
        const SizedBox(width: 16), // Espacement entre le nom de l'utilisateur et le panier
        Consumer<Cart>(
          builder: (context, cart, child) => Stack(
            children: [
              IconButton(
                icon: const Icon(Icons.shopping_cart),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const CartDetails(),
                    ),
                  );
                },
              ),
              Positioned(
                top: 5,
                right: 5,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Colors.red,
                  child: Text(
                    cart.totalItem.toString(),
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
