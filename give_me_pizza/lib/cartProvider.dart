import 'dart:collection';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'pizzas.dart';

class Cart extends ChangeNotifier {
  final Map<Pizzas, int> _items = {}; // la map va associé les infos d'une pizza avec un entier qui représente sa quantité

  UnmodifiableMapView<Pizzas, int> get items => UnmodifiableMapView(_items);

  int  totalItem = 0;
  String ?lastOrder;
  int get totalPrice {
    int totalPrice = 0;
    _items.forEach((pizza, quantity) {
      totalPrice += pizza.price * quantity;
    });
    return totalPrice;
  }

void add(Pizzas pizza) {
  if (_items.containsKey(pizza)) {
    int currentQuantity = _items[pizza] ?? 0;
    
    _items[pizza] = currentQuantity + 1; // Incrémente la quantité si la pizza est déjà présente
  } else {
    _items[pizza] = 1; // Ajoute une nouvelle entrée si la pizza n'est pas déjà présente
  }
  totalItem += 1;
  notifyListeners();
}


void remove(Pizzas pizza) {
  if (_items.containsKey(pizza)) {
    if (_items[pizza] == 1) {
      _items.remove(pizza); // Supprime la pizza si sa quantité est de 1
    } else {
      _items[pizza] = _items[pizza]! - 1; // Décrémente la quantité sinon
    }
    totalItem -= 1;
    notifyListeners();
  }
}


  void removeAll() {
    _items.clear();
    totalItem = 0;
    notifyListeners();
  }

  String cartToJson() {
    List<Map<String, dynamic>> pizzasJson = _items.entries.map((entry) {
      final pizza = entry.key;
      final quantity = entry.value;
      return {
        'pizza_id': pizza.id,
        'quantity': quantity,
      };
    }).toList();
    print(jsonEncode(pizzasJson));
    return jsonEncode(pizzasJson);
  }

  Future<int?> sendCart(accessToken) async {

    final data = {
    '"orderlines"': cartToJson()
    };
    print(data);
    try {
      final Response response = await Dio().post(
        'https://pizzas.shrp.dev/items/orders',
         options: Options(headers: {'Authorization': 'Bearer $accessToken'}),
        data: data,
      );
      if (response.statusCode == 200) {
        removeAll();
        lastOrder = response.data['data']['id'];

      } else {
        throw Exception('Erreur lors de l\'inscription');
      }
      print(response.statusCode);
      print(response.data);
      return response.statusCode;
    } catch (error) {
      print('Erreur lors de la requête: $error');
      throw Exception('Erreur lors de l\'inscription');
    }
    
  }
  

}



