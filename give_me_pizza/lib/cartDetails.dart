import 'package:flutter/material.dart';
import 'package:give_me_pizza/cartProvider.dart';
import 'package:give_me_pizza/navbar.dart';
import 'package:give_me_pizza/user_provider.dart';
import 'package:provider/provider.dart';



class CartDetails extends StatelessWidget {
  const CartDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomNavbar().getAppBar(context, 'Mon pannier'),
      body: Column(
        children: [
          Consumer<UserProvider>(
            builder: (context, userProvider, child) {
              if (userProvider.user.accessToken != null) {
                // Si l'utilisateur est connecté, affichez le bouton "Valider ma commande"
                return ElevatedButton(
                  onPressed: () async {
                    int? message = await Provider.of<Cart>(context, listen: false).sendCart(userProvider.user.accessToken);
                    if (message != 200) {
                      // Afficher un message d'erreur
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(message.toString()),
                          backgroundColor: Colors.red,
                        ),
                      );
                    } else {
                      // Afficher un message de succès
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('La commande a été passée avec succès.'),
                          backgroundColor: Colors.green,
                        ),
                      );
                    }
                  },
                  child: const Text('Valider ma commande'),
                );
              } else {
                // Si l'utilisateur n'est pas connecté, affichez le message "Connectez-vous pour passer votre commande"
                return const Text('Connectez-vous pour passer votre commande');
              }
            },
          ),
          Expanded(
            child: Consumer<Cart>(
              builder: (context, cart, child) {
                return ListView(
                  children: [
                    ListTile(
                      title: Text('Prix total : ${cart.totalPrice} €'),
                      // You can style this line as needed
                    ),
                    ...cart.items.entries.map((entry) {
                      final pizza = entry.key;
                      final quantity = entry.value;
                      return ListTile(
                        title: Text(pizza.name),
                        subtitle: Text('Quantité : $quantity, Prix unitaire : ${pizza.price} €'),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            IconButton(
                              icon: const Icon(Icons.remove),
                              onPressed: () {
                                cart.remove(pizza);
                              },
                            ),
                            Text('$quantity'),
                            IconButton(
                              icon: const Icon(Icons.add),
                              onPressed: () {
                                cart.add(pizza);
                              },
                            ),
                          ],
                        ),
                      );
                    }),
                    // Ajoutez le bouton "S'inscrire pour passer commande" ici
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}


