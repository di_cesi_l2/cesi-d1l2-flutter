import 'package:flutter/material.dart';
import 'package:give_me_pizza/user_provider.dart';
import 'package:provider/provider.dart';

class SignupForm extends StatelessWidget {
  const SignupForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of<UserProvider>(context);
        TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    TextEditingController firstnameController = TextEditingController();
    TextEditingController lastnameController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Inscription'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextFormField(
                controller: emailController,
                decoration: const InputDecoration(labelText: 'Email'),
                
              ),
              TextFormField(
                controller: firstnameController,
                decoration: const InputDecoration(labelText: 'Prénom'),
        
              ),
              TextFormField(
                controller: lastnameController,
                decoration: const InputDecoration(labelText: 'Nom'),
       
              ),
              TextFormField(
                controller: passwordController,
                decoration: const InputDecoration(labelText: 'Mot de passe'),
                obscureText: true,
              
              ),
              ElevatedButton(
                onPressed: () {
                  userProvider.signup(
                    emailController.text,
                    passwordController.text,
                    firstnameController.text,
                    lastnameController.text

                  );
                },
                child: const Text('S\'inscrire'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}



