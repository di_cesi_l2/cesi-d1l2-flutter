import 'package:flutter/material.dart';
import 'package:give_me_pizza/cartProvider.dart';
import 'package:give_me_pizza/navbar.dart';
import 'package:give_me_pizza/pizzas.dart';
import 'package:provider/provider.dart';
class PizzaList extends StatefulWidget {
  const PizzaList({super.key, required this.title});

  final String title;

  @override
  State<PizzaList> createState() => _PizzaListState();
}

class _PizzaListState extends State<PizzaList> {
  late Future<List<Pizzas>> pizzas;

  @override
  void initState() {
    super.initState();
    pizzas = getPizzas();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomNavbar().getAppBar(context,'Ma liste de pizza'),
      body: Center(
        child: FutureBuilder<List<Pizzas>>(
          future: pizzas,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: Image.network(
                      snapshot.data![index].image,
                      width: 50, 
                      height: 50, 
                    ),
                    title: Text(snapshot.data![index].name),
                    subtitle: Text(snapshot.data![index].category),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => PizzaDetailsPage(pizza: snapshot.data![index]),
                        ),
                      );
                    },
                  );
                },
              );
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            } else {
              return const CircularProgressIndicator();
            }
          },
        ),
      ),
    );
      
      
  }
}

class PizzaDetailsPage extends StatelessWidget {
  final Pizzas pizza;

  const PizzaDetailsPage({super.key, required this.pizza});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomNavbar().getAppBar(context,'Détail de ma pizza'),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Ingrédients:',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            const SizedBox(height: 8),
            
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: pizza.ingredients.map((ingredient) {
                return Text('- $ingredient');
              }).toList(),
            ),
            Text('Prix : ${pizza.price} €'),
                        ElevatedButton(
onPressed: () {
  Provider.of<Cart>(context, listen: false).add(pizza);

},
              child: const Text('Ajouter au panier'),
            ),
          ],
        ),
      ),
    );
  }
}


